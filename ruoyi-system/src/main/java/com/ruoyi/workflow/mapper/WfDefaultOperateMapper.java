package com.ruoyi.workflow.mapper;

import com.ruoyi.workflow.domain.WfDefaultOperate;
import com.ruoyi.workflow.domain.vo.WfDefaultOperateVo;
import com.ruoyi.common.core.mapper.BaseMapperPlus;

/**
 * 流程默认操作Mapper接口
 *
 * @author nbacheng
 * @date 2023-11-23
 */
public interface WfDefaultOperateMapper extends BaseMapperPlus<WfDefaultOperateMapper, WfDefaultOperate, WfDefaultOperateVo> {

}
